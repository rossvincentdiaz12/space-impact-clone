﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour {
    public float speed = 10.0f;
    private Rigidbody2D rb;
    private Vector2 screenBounds;
    public AudioClip crash;

    public playercontrol player;//to find the 

    // Use this for initialization
    void Start () {
        //to find the object where the player manager script were
		player=FindObjectOfType<playercontrol>();
        rb = this.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(speed, 0);
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    // Update is called once per frame
    void Update () {
        if(transform.position.x > screenBounds.x ){
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D col){
        if (col.gameObject.tag.Equals("enemy")){
            Destroy(col.gameObject);
            Destroy(this.gameObject);
            AudioSource.PlayClipAtPoint(crash, transform.position);
            player.count = player.count + 100;
            player.SetCountText();
        }
        if (col.gameObject.tag.Equals("enemy2")){
            Destroy(col.gameObject);
            Destroy(this.gameObject);
            AudioSource.PlayClipAtPoint(crash, transform.position);
            player.count = player.count + 200;
            player.SetCountText();
        }
    }
}